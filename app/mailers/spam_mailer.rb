class SpamMailer < ActionMailer::Base
  def send_mail(options)
    @mail_content = options['body']
    mail(to: options['to'], subject: options['subject'], body: @mail_content, from: 'Nobody<noreply@example.com>').deliver!
  end
end
