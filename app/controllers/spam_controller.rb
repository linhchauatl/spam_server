class SpamController < ApplicationController
  def create
    begin
      missing_attributes = []

      [:to, :subject, :body].each do |param|
        missing_attributes << param unless params[param].present?
      end

      raise "Required attributes missing: #{missing_attributes.join(',')}" unless missing_attributes.empty?

      SpamMailer.send_mail(params)

      render text: 'Message sent'
    rescue => error
      Rails.logger.error("#{error.message}\n#{error.backtrace.join("\n")}")
      render text: "Boom! #{error.message}!", status: 500
    end
  end
end