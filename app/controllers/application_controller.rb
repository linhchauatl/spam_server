class ApplicationController < ActionController::Base

  before_filter :authenticate

  def authenticate
    require "#{Rails.root}/config/auth_token"
    render text: 'Unauthorized', status: 401 unless (params[:auth_token] == AUTH_TOKEN)
    return (params[:auth_token] == AUTH_TOKEN)
  end
end
