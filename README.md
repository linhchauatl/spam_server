#Spam Server

This is a solution for the following interview question:

**Exercise:**
Create an API endpoint that sends an email when someone submits a POST request with JSON content.

Sample payload:<br/>
```
{ "to": "James Hwang <jhwang@newrelic.com>", "subject": "Hello, world!", "body": "Hi James! Sending you an email via this API I just made." }
```

**Requirements:**
- set the subject, body, and to address to the values provided in the JSON payload
- send email from noreply@example.com
- put this on the real internet and send real email; we want to play with it!
- use git + github; commit early and often
- use ruby or a ruby-based framework: Sinatra or Rails

**More things to know:**
- Do not commit any sensitive information to your repo (usernames, passwords, api keys)
- This is just an API; you don't need (and shouldn't build) an html front-end.
- In addition to the technologies mentioned above, feel free to use any gems/frameworks that will make the task easier.
- Send us your web app url and a link to your repo when you are finished.
- When we're done chatting remember to take your app offline, so no spammers use it to spew spam using your API!
