require 'rails_helper'

describe SpamMailer do
  describe 'sendmail' do
    before :all do
      @options = { 'to' => 'Test Recipient <test@nowhere.com>', 
                   'subject' => 'Hello, world!', 
                   'body' => 'Hi Tester! Sending you an email via this API I just made.'
                 }
    end

    let(:mail) { SpamMailer.send_mail(@options) }

    it 'renders the recipient' do
      expect(mail.to).to eql(['test@nowhere.com'])
    end

    it 'renders the subject' do
       expect(mail.subject).to eql('Hello, world!')
    end

    it 'renders the body' do
      expect(mail.body).to match('Hi Tester! Sending you an email via this API I just made.')
    end
  end
end