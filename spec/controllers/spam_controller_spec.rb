require 'rails_helper'

describe SpamController do
  describe 'create' do 
    
    context 'success' do
      before :all do
        @data = { 'to' => 'Test Recipient <test@nowhere.com>', 
                  'subject' => 'Hello, world!', 
                  'body' => 'Hi Tester! Sending you an email via this API I just made.',
                  'auth_token' => AUTH_TOKEN
                }
      end

      it "responds successfully with an HTTP 200 status code" do
        post :create, @data
        expect(response).to be_success
        expect(response).to have_http_status(200)
      end
    end

    context 'unauthorized' do
      it 'responds with unauthorized 401' do
        post :create # No authenticate token
        expect(response).to have_http_status(401)
      end
    end

    context 'failure' do
      it 'responds with 500 if there is no recipient' do
        post :create, { 'subject' => 'Hello, world!', 
                  'body' => 'Hi Tester! Sending you an email via this API I just made.',
                  'auth_token' => AUTH_TOKEN
                }

        expect(response).to have_http_status(500)

      end

      it 'responds with 500 if there is no subject' do
        post :create, { 'to' => 'Test Recipient <test@nowhere.com>', 
                  'body' => 'Hi Tester! Sending you an email via this API I just made.',
                  'auth_token' => AUTH_TOKEN
                }
        expect(response).to have_http_status(500) 
      end

      it 'responds with 500 if there is no body' do
        post :create, { 'to' => 'Test Recipient <test@nowhere.com>', 
                  'subject' => 'Hello, world!',
                  'auth_token' => AUTH_TOKEN
                }
        expect(response).to have_http_status(500)
      end
    end

  end  
end